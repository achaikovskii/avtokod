from .base_page import BasePage
from .locators import NotificationLocators
from .locators import KSKLocators
from .locators import RepairCarLocators
from .locators import TableFinesLocators
from .locators import GibddAppointmentLocators
from .locators import MadiLocators
from .locators import MadiAppealsLocators
from .locators import CheckStatusAppealsPageLocators
from .locators import SchemeAppealsLocators
from .locators import GenerateContractLocators
from .locators import InsurancePageLocators
from .locators import MosruLkLocators


class NotificationPage(BasePage):
    def should_be_major_elements(self):
        assert self.browser.find_element(*NotificationLocators.TITLE), 'Не отобразился заголовок'
        assert self.browser.find_element(*NotificationLocators.GO_TO_MOSRU), 'Ссылка на переход в ЛК не доступна'

    def go_to_mosru(self):
        self.browser.find_element(*NotificationLocators.GO_TO_MOSRU).click()
        assert self.browser.find_element(*MosruLkLocators.TITLE)


class KskPage(BasePage):
    def should_be_btn_new_kasko(self):
        assert self.browser.find_element(*KSKLocators.BTN_NEW_KSK), \
            'Кнопка "Рассчитать стоимость нового полиса" не доступна'

    def should_be_btn_long_kasko(self):
        assert self.browser.find_element(*KSKLocators.BTN_LONG_KSK), \
            'Кнопка "Рассчитать продление" не доступна'


class RepairCar(BasePage):
    def should_be_btn_new_kasko(self):
        assert self.browser.find_element(*RepairCarLocators.BTN_GET_PRICE_REPAIR), \
            'Кнопка "Рассчитать стоимость ремонта" не доступна'


class TableFines(BasePage):
    def should_page_loading(self):
        assert self.browser.find_element(*TableFinesLocators.SUCCESS_LOAD_FLAG1), \
            'Страница таблиц штрафов не прогружена'
        assert self.browser.find_element(*TableFinesLocators.SUCCESS_LOAD_FLAG2), \
            'Страница таблиц штрафов не прогружена'


class GibddAppointment(BasePage):
    def should_be_btn_get_appointment_gibdd(self):
        assert self.browser.find_element(*GibddAppointmentLocators.BTN_GET_APPOINTMENT), \
            'Кнопка предзаписи в ГИБДД не доступна'

    def should_be_btn_get_appointment_medical(self):
        assert self.browser.find_element(*GibddAppointmentLocators.BTN_GET_APPOINTMENT_MEDICAL), \
            'Кнопка предзаписи на медкомиссию не доступна'


class MadiAppointment(BasePage):
    def should_be_btn_get_appointment_madi(self):
        assert self.browser.find_element(*MadiLocators.BTN_GET_APPOINTMENT), \
            'Кнопка предзаписи МАДИ не доступна'


class MadiAppeals(BasePage):
    def should_be_all_reasons_for_appeals(self):
        assert self.browser.find_element(*MadiAppealsLocators.APPEALS_MADI_TYPE_1), \
            'Жалоба по постановлению об АП не доступна'
        assert self.browser.find_element(*MadiAppealsLocators.APPEALS_MADI_TYPE_2), \
            'Уведомление об оплате штрафа не доступно'
        assert self.browser.find_element(*MadiAppealsLocators.APPEALS_MADI_TYPE_3), \
            'Возврат денег за оплаченный штраф не доступен'
        assert self.browser.find_element(*MadiAppealsLocators.APPEALS_MADI_TYPE_4), \
            'Запрос копии постановления не доступен'
        assert self.browser.find_element(*MadiAppealsLocators.APPEALS_MADI_TYPE_5), \
            'Сообщение об отмене постановления не доступно'
        assert self.browser.find_element(*MadiAppealsLocators.APPEALS_MADI_TYPE_6), \
            'Предоставление доп материалов по обращению не доступно'


class CheckAppealPage(BasePage):
    def should_be_major_elements(self):
        assert self.browser.find_element(*CheckStatusAppealsPageLocators.INPUT_UIN), \
            'Нет поля УИН'
        assert self.browser.find_element(*CheckStatusAppealsPageLocators.BTN_CHECK_APPEAL_UIN), \
            'Нет кнопки проверки УИН'
        assert self.browser.find_element(*CheckStatusAppealsPageLocators.INPUT_APPEAL), \
            'Нет поля номера обжалования'
        assert self.browser.find_element(*CheckStatusAppealsPageLocators.BTN_CHECK_APPEAL), \
            'Нет кнопки проверки по номеру обжалования'


class SchemeAppealPage(BasePage):
    def should_be_major_elements(self):
        assert self.browser.find_element(*SchemeAppealsLocators.INPUT_UIN), 'Нет поля ввода УИН'
        assert self.browser.find_element(*SchemeAppealsLocators.BTN_CHECK_UIN), 'Нет кнопки проверки по номеру УИН'


class GenerateContractPage(BasePage):
    def should_be_major_elements(self):
        assert self.browser.find_element(*GenerateContractLocators.CONTRACT_FORM), 'Форма договора не отобразилась'
        assert self.browser.find_element(*GenerateContractLocators.CONTRACT_BTN), \
            'Кнопка "Скачать договор" не отобразилась'


class InshurancePage(BasePage):
    def should_be_major_elements(self):
        assert self.browser.find_element(*InsurancePageLocators.INSHUR_MENU), 'Нет меню выбора страхования'
        assert self.browser.find_element(*InsurancePageLocators.KASKO_BTN), 'Нет кнопки "Рассчитать каско"'
        assert self.browser.find_element(*InsurancePageLocators.OSAGO_BTN), 'Нет кнопки "Рассчитать ОСАГО"'
        assert self.browser.find_element(*InsurancePageLocators.NEW_POLIS_BTN), 'Нет кнопки "Оформить новый полис"'
