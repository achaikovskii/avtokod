from .base_page import BasePage
from .locators import AmppAppealsLocators
from time import sleep
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec


class AmppAppeals(BasePage):
    def should_be_all_reasons_for_appeals(self):
        assert self.browser.find_element(*AmppAppealsLocators.APPEALS_AMPP_TYPE_1), \
            'Жалоба по постановлению об АП не доступна'
        assert self.browser.find_element(*AmppAppealsLocators.APPEALS_AMPP_TYPE_2), \
            'Предоставление доп. материалов по обращению не доступно'

    def fill_appeal_data_step_one(self):
        self.browser.find_element(*AmppAppealsLocators.NUMBER_UIN).send_keys(AmppAppealsLocators.TEST_UIN)
        self.browser.find_element(*AmppAppealsLocators.DATE_UIN).send_keys(AmppAppealsLocators.TEST_DATE)
        self.browser.find_element(*AmppAppealsLocators.REASON_APPEAL).click()
        self.browser.find_element(*AmppAppealsLocators.REASON_APPEAL_LIST).click()

    def go_next_step(self):
        sleep(3)
        WebDriverWait(self.browser, 10).until(
            ec.element_to_be_clickable(AmppAppealsLocators.GO_NEXT_STEP_BUTTON)
        ).click()

    def go_to_appeal_ap(self):
        self.browser.find_element(*AmppAppealsLocators.APPEALS_AMPP_TYPE_1).click()

    def click_ok_on_the_info_page(self):
        if self.browser.find_element(*AmppAppealsLocators.AGREE_MESSAGE):
            self.browser.find_element(*AmppAppealsLocators.AGREE_MESSAGE).click()
        else:
            pass

    def should_be_open_form_two(self):
        assert 'Ampp/Ruling/step2' in self.browser.current_url, 'Произошел переход не на 2 форму'

    def check_disable_button_next_step_one(self):
        assert not self.browser.find_element(*AmppAppealsLocators.GO_NEXT_STEP_BUTTON).is_enabled(), 'Кнопка активна'
