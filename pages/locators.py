from selenium.webdriver.common.by import By


class BasePageLocators:
    URL = 'https://avtokod.mos.ru/'
    BTN_LOGIN = (By.CSS_SELECTOR, '.user__block>a')
    BTN_LOGOUT = (By.CSS_SELECTOR, '.user__block>a:nth-child(4)')
    FORM_MOSRU_LOGIN = (By.ID, 'login')
    FORM_MOSRU_PASSWORD = (By.ID, 'password')
    FORM_MOSRU_LOGIN_BTN = (By.CSS_SELECTOR, 'button#bind')
    FORM_MOSRU_SUCCESS_LOGOUT_FLAG = (By.CLASS_NAME, 'fs14')
    USERNAME = '***'
    PASSWORD = '***'
    SUCCESS_LOGIN_FLAG = (By.CSS_SELECTOR, '.logotype.user__info')


class NotificationLocators:
    URL = 'https://avtokod.mos.ru/Pages/Personal/Cabinet.aspx#tab5'
    TITLE = (By.CLASS_NAME, 'settings__header-title')
    GO_TO_MOSRU = (By.CSS_SELECTOR, 'div:nth-child(2)>p')


class KSKLocators:
    URL = 'https://avtokod.mos.ru/Pages/el-polis/kasko/'
    BTN_NEW_KSK = (By.CSS_SELECTOR, 'a.button.info')
    BTN_LONG_KSK = (By.CSS_SELECTOR, 'button.button.info')


class RepairCarLocators:
    URL = 'https://avtokod.mos.ru/Pages/Autoservices.aspx'
    BTN_GET_PRICE_REPAIR = (By.CSS_SELECTOR, 'div>.avtokod-get-price-btn')


class CheckFinesLocators:
    URL = 'https://avtokod.mos.ru/Fines/Info'
    TEST_DAP = '0355431010121112201039805'
    INVALID_DAP = '035543101012111220103980'
    INVALID_DAP_MESSAGE = (By.CSS_SELECTOR, '#valideMessage_3>.sign')
    TEST_VU = '4510399320'
    INVALID_VU = '451039932'
    INVALID_VU_MESSAGE = (By.CSS_SELECTOR, '#valideMessage_0>.sign')
    TEST_DATE = '17.02.2015'
    INVALID_DATE = '17.02.201'
    TEST_STS = '5060327524'
    INVALID_STS = '506032752'
    INVALID_STS_MESSAGE = (By.CSS_SELECTOR, '#valideMessage_1>.sign')
    INPUT_VU = (By.ID, 'fieldVu')
    INPUT_VU_DATE = (By.ID, 'fieldDate')
    INPUT_STS = (By.ID, 'fieldTc')
    INPUT_UIN = (By.ID, 'fieldDAP')
    BTN_CHECK_VU_STS = (By.ID, 'linkcheckData')
    BTN_CHECK_DAP = (By.ID, 'linkcheckDataDAP')
    RESULT_GUEST_FLAG = (By.CSS_SELECTOR, 'button#bind')
    REDIRECT_MOSPAY_FLAG = (By.CLASS_NAME, 'tooltip_reg')


class TableFinesLocators:
    URL = 'https://avtokod.mos.ru/Pages/PenaltiesTable.aspx'
    SUCCESS_LOAD_FLAG1 = (By.ID, 'header_caption')
    SUCCESS_LOAD_FLAG2 = (By.CLASS_NAME, 'abbreviation_div')


class GibddAppointmentLocators:
    URL = 'https://avtokod.mos.ru/Record/Info/GIBDD'
    URL_MEDICAL = 'https://avtokod.mos.ru/Pages/InfoQueueMed.aspx'
    BTN_GET_APPOINTMENT = (By.CLASS_NAME, 'button-area__queue-button')
    BTN_GET_APPOINTMENT_MEDICAL = (By.CSS_SELECTOR, '.block_enroll>a')


class MadiLocators:
    URL = 'https://avtokod.mos.ru/Appeals/MADI/#AppointmentMADI'
    BTN_GET_APPOINTMENT = (By.CLASS_NAME, 'appeal-info__button--3d')


class AmppAppealsLocators:
    URL = 'https://avtokod.mos.ru/Appeals/AMPP/'
    APPEALS_AMPP_TYPE_1 = (By.XPATH, '//div[@class="appeal-info__content-item"][1]/a')
    APPEALS_AMPP_TYPE_2 = (By.XPATH, '//div[@class="appeal-info__content-item"][2]/a')

    NUMBER_UIN = (By.CSS_SELECTOR, 'input[placeholder="12345678901234567890"]')
    TEST_UIN = '0355431010122041501013123'
    INVALID_UIN = '035543101012204150101312'
    DATE_UIN = (By.CSS_SELECTOR, 'input[placeholder="01.01.2022"]')
    TEST_DATE = '17.05.2022'
    INVALID_DATE = '17.05.202'
    REASON_APPEAL = (By.CSS_SELECTOR, 'div.items')
    REASON_APPEAL_LIST = (By.CSS_SELECTOR, 'label[for="list[0]"]')
    AGREE_MESSAGE = (By.CSS_SELECTOR, '.next_btn>shared-ui-button>button.button')
    GO_NEXT_STEP_BUTTON = (By.CSS_SELECTOR, 'button[type="submit"]')


class MadiAppealsLocators:
    URL = 'https://avtokod.mos.ru/Appeals/MADI/'
    APPEALS_MADI_TYPE_1 = (By.XPATH, '//div[@class="appeal-info__content-item"][1]/a')
    APPEALS_MADI_TYPE_2 = (By.XPATH, '//div[@class="appeal-info__content-item"][2]/a')
    APPEALS_MADI_TYPE_3 = (By.XPATH, '//div[@class="appeal-info__content-item"][3]/a')
    APPEALS_MADI_TYPE_4 = (By.XPATH, '//div[@class="appeal-info__content-item"][4]/a')
    APPEALS_MADI_TYPE_5 = (By.XPATH, '//div[@class="appeal-info__content-item"][5]/a')
    APPEALS_MADI_TYPE_6 = (By.XPATH, '//div[@class="appeal-info__content-item"][6]/a')


class CheckStatusAppealsPageLocators:
    URL = 'https://avtokod.mos.ru/Appeals/#AppealsAnchor'
    INPUT_UIN = (By.ID, 'appealNumber')
    INPUT_APPEAL = (By.ID, 'formNumberText')
    BTN_CHECK_APPEAL_UIN = (By.XPATH, '//div[@class="status-appeal__form"]/button')
    BTN_CHECK_APPEAL = (By.CSS_SELECTOR, '.form__control-block>button')


class SchemeAppealsLocators:
    URL = 'https://avtokod.mos.ru/Appeals/AppealScheme'
    INPUT_UIN = (By.CLASS_NAME, 'appeal-type__check-input')
    BTN_CHECK_UIN = (By.CLASS_NAME, 'appeal-type__check-button')


class FeedBackLocators:
    URL = 'https://avtokod.mos.ru/Pages/FeedBack.aspx'


class GenerateContractLocators:
    URL = 'https://avtokod.mos.ru/PurchaseAgreement'
    CONTRACT_FORM = (By.CSS_SELECTOR, '.column.verybig')
    CONTRACT_BTN = (By.CSS_SELECTOR, 'input.btn-big-green')


class InsurancePageLocators:
    URL = 'https://avtokod.mos.ru/Pages/CarInsurance.aspx'
    INSHUR_MENU = (By.CLASS_NAME, 'tabs-tablist')
    OSAGO_BTN = (By.XPATH, '//li[@class="calc-pay-product"][1]/a')
    KASKO_BTN = (By.XPATH, '//li[@class="calc-pay-product"][2]/a')
    NEW_POLIS_BTN = (By.CSS_SELECTOR, '.tCenter.osago-eosago-process>a')


class MosruLkLocators:
    URL = 'https://my.mos.ru/my/#/settings/profile'
    TITLE = (By.CLASS_NAME, 'profile__heading')


class APILocators:
    TOKEN = {"nonce":"bot","systemid":"emp","timestamp":"1653047088","token":"***"}
    UIN = '0356043010119091300030071'

    URL_GET_FINES = 'http://avtokodapi.mos.ru:7102/api/Penalties/Get'
    URL_GET_PAID_FINES = 'http://avtokodapi.mos.ru:7102/api/Penalties/GetPaid'

    URL_GET_FINES_JSON = {
        "securityInfoData":
            TOKEN,
        "limit": "10",
        "offset": 0
    }

    URL_GET_PHOTO = 'http://avtokodapi.mos.ru:7102/v2/api/PenaltyPhotos/Get'
    URL_GET_PHOTO_JSON = {
        "securityInfoData" : TOKEN,
        "rulingNumber": UIN
    }
