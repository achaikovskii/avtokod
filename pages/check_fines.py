import pytest
from .base_page import BasePage
from .locators import CheckFinesLocators
from selenium.webdriver.common.action_chains import ActionChains


class CheckFines(BasePage):
    @pytest.mark.smoke
    def should_be_major_elements(self):
        assert self.browser.find_element(*CheckFinesLocators.INPUT_VU), 'Поле ввода VU не доступно'
        assert self.browser.find_element(*CheckFinesLocators.INPUT_VU_DATE), 'Поле ввода даты не доступно'
        assert self.browser.find_element(*CheckFinesLocators.INPUT_STS), 'Поле ввода СТС не доступно'
        assert self.browser.find_element(*CheckFinesLocators.BTN_CHECK_VU_STS), 'Кнопка поиска ВУ/СТС не доступна'
        assert self.browser.find_element(*CheckFinesLocators.INPUT_UIN), 'Поле ввода УИН не доступно'
        assert self.browser.find_element(*CheckFinesLocators.BTN_CHECK_DAP), 'Кнопка поиска по УИН не доступна'

    def check_fine_uin(self):
        self.browser.find_element(*CheckFinesLocators.INPUT_UIN).send_keys(CheckFinesLocators.TEST_DAP)
        self.browser.find_element(*CheckFinesLocators.BTN_CHECK_DAP).click()

    def check_fine_vu(self):
        self.browser.find_element(*CheckFinesLocators.INPUT_VU).send_keys(CheckFinesLocators.TEST_VU)
        self.browser.find_element(*CheckFinesLocators.INPUT_VU_DATE).send_keys(CheckFinesLocators.TEST_DATE)
        self.browser.find_element(*CheckFinesLocators.BTN_CHECK_VU_STS).click()

    def check_fine_sts(self):
        self.browser.find_element(*CheckFinesLocators.INPUT_STS).send_keys(CheckFinesLocators.TEST_STS)
        self.browser.find_element(*CheckFinesLocators.BTN_CHECK_VU_STS).click()

    def should_be_message_invalid_dap(self):
        self.browser.find_element(*CheckFinesLocators.INPUT_UIN).send_keys(CheckFinesLocators.INVALID_DAP)
        self.browser.find_element(*CheckFinesLocators.BTN_CHECK_DAP).click()
        assert self.browser.find_element(*CheckFinesLocators.INVALID_DAP_MESSAGE)

    def should_be_message_invalid_vu(self):
        self.browser.find_element(*CheckFinesLocators.INPUT_VU).clear()
        self.browser.find_element(*CheckFinesLocators.INPUT_STS).clear()
        self.browser.find_element(*CheckFinesLocators.INPUT_VU).send_keys(CheckFinesLocators.INVALID_VU)
        self.browser.find_element(*CheckFinesLocators.BTN_CHECK_VU_STS).click()
        assert self.browser.find_element(*CheckFinesLocators.INVALID_VU_MESSAGE)

    def should_be_message_invalid_sts(self):
        # ActionChains(self.browser).move_to_element(
        #     self.browser.find_element(*CheckFinesLocators.BTN_CHECK_VU_STS)).perform()
        self.browser.find_element(*CheckFinesLocators.INPUT_VU).clear()
        self.browser.find_element(*CheckFinesLocators.INPUT_STS).clear()
        self.browser.find_element(*CheckFinesLocators.INPUT_STS).send_keys(CheckFinesLocators.INVALID_STS)
        self.browser.find_element(*CheckFinesLocators.BTN_CHECK_VU_STS).click()
        assert self.browser.find_element(*CheckFinesLocators.INVALID_STS_MESSAGE)

    def check_result_for_guest(self):
        assert self.browser.find_element(*CheckFinesLocators.RESULT_GUEST_FLAG), \
            'Переход на форму регистрации не выполнен'

    def check_redirect(self):
        assert self.browser.find_element(*CheckFinesLocators.REDIRECT_MOSPAY_FLAG), \
            'Переадресация на Мои платежи не найдена'