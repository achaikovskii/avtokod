from .locators import BasePageLocators
import allure
from allure_commons.types import AttachmentType


class BasePage:
    def __init__(self, browser, url, timeout=20):
        self.browser = browser
        self.url = url
        self.browser.implicitly_wait(timeout)

    def open(self):
        self.browser.get(self.url)
        with allure.step('Screenshot'):
            allure.attach(self.browser.get_screenshot_as_png(), name="Screenshot", attachment_type=AttachmentType.PNG)

    def login(self):
        self.browser.find_element(*BasePageLocators.BTN_LOGIN).click()
        if self.browser.find_element(*BasePageLocators.FORM_MOSRU_LOGIN):
            self.browser.find_element(*BasePageLocators.FORM_MOSRU_LOGIN).send_keys(BasePageLocators.USERNAME)
            self.browser.find_element(*BasePageLocators.FORM_MOSRU_PASSWORD).send_keys(BasePageLocators.PASSWORD)
            self.browser.find_element(*BasePageLocators.FORM_MOSRU_LOGIN_BTN).click()
        else:
            self.browser.find_element(*BasePageLocators.FORM_MOSRU_PASSWORD).send_keys(BasePageLocators.PASSWORD)
            self.browser.find_element(*BasePageLocators.FORM_MOSRU_LOGIN_BTN).click()
        assert self.browser.find_element(*BasePageLocators.SUCCESS_LOGIN_FLAG), 'Login failed'

    def logout(self):
        if self.browser.find_element(*BasePageLocators.SUCCESS_LOGIN_FLAG):
            self.browser.find_element(*BasePageLocators.BTN_LOGOUT).click()
            self.browser.get(BasePageLocators.URL)
        assert self.browser.find_element(*BasePageLocators.BTN_LOGIN), 'Logout failed'