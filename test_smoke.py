import pytest
from .pages.base_page import BasePage
from .pages.smoke import KskPage
from .pages.smoke import RepairCar
from .pages.smoke import TableFines
from .pages.smoke import GibddAppointment
from .pages.smoke import MadiAppointment
from .pages.smoke import MadiAppeals
from .pages.smoke import CheckAppealPage
from .pages.smoke import SchemeAppealPage
from .pages.smoke import GenerateContractPage
from .pages.smoke import InshurancePage
from .pages.smoke import NotificationPage
from .pages.locators import BasePageLocators
from .pages.locators import KSKLocators
from .pages.locators import RepairCarLocators
from .pages.locators import TableFinesLocators
from .pages.locators import GibddAppointmentLocators
from .pages.locators import MadiLocators
from .pages.locators import MadiAppealsLocators
from .pages.locators import CheckStatusAppealsPageLocators
from .pages.locators import SchemeAppealsLocators
from .pages.locators import GenerateContractLocators
from .pages.locators import InsurancePageLocators
from .pages.locators import NotificationLocators


"""Доступность кнопки оформления каско"""
@pytest.mark.smoke
def test_guest_btn_new_is_available(browser):
    page = KskPage(browser, KSKLocators.URL)
    page.open()
    page.should_be_btn_new_kasko()


"""Доступность кнопки продления каско"""
@pytest.mark.smoke
def test_guest_btn_long_is_available(browser):
    page = KskPage(browser, KSKLocators.URL)
    page.open()
    page.should_be_btn_long_kasko()


"""Доступность кнопки записи на ремонт ТС"""
@pytest.mark.smoke
def test_guest_btn_get_price_repair_car(browser):
    page = RepairCar(browser, RepairCarLocators.URL)
    page.open()
    page.should_be_btn_new_kasko()


"""Полная прогрузка страницы таблицы штрафов"""
@pytest.mark.smoke
def test_guest_loading_table_fines(browser):
    page = TableFines(browser, TableFinesLocators.URL)
    page.open()
    page.should_page_loading()


"""Доступность кнопки записи на приём в ГИБДД"""
@pytest.mark.smoke
def test_guest_btn_get_appointment_gibdd_is_available(browser):
    page = GibddAppointment(browser, GibddAppointmentLocators.URL)
    page.open()
    page.should_be_btn_get_appointment_gibdd()


"""Доступность кнопки записи на приём в МАДИ"""
@pytest.mark.smoke
def test_guest_btn_get_appointment_madi__is_available(browser):
    page = MadiAppointment(browser, MadiLocators.URL)
    page.open()
    page.should_be_btn_get_appointment_madi()


"""Доступность кнопки записи на приём на медкомиссию на странице 'Запись на медкомиссию'"""
@pytest.mark.smoke
def test_guest_btn_get_appointment_gibdd_is_available(browser):
    page = GibddAppointment(browser, GibddAppointmentLocators.URL_MEDICAL)
    page.open()
    page.should_be_btn_get_appointment_medical()


"""Доступность всех причин обжалования для страницы жалоб МАДИ"""
@pytest.mark.smoke
def test_should_be_all_reasons_madi_for_appeals(browser):
    page = MadiAppeals(browser, MadiAppealsLocators.URL)
    page.open()
    page.should_be_all_reasons_for_appeals()


"""Авторизация пользователя"""
@pytest.mark.smoke
def test_login(browser):
    page = BasePage(browser, BasePageLocators.URL)
    page.open()
    page.login()


"""Разлогинивание пользователя"""
@pytest.mark.smoke
def test_login(browser):
    page = BasePage(browser, BasePageLocators.URL)
    page.open()
    page.login()
    page.logout()


"""Доступность всех ключевых элементов на странице проверки обжалования"""
@pytest.mark.smoke
def test_should_be_major_elements_check_appeals_page(browser):
    page = CheckAppealPage(browser, CheckStatusAppealsPageLocators.URL)
    page.open()
    page.should_be_major_elements()


"""Доступность элементов взаимодействия с пользователем на странице схемы обжалования"""
@pytest.mark.smoke
def test_should_be_major_elements_scheme_appeals_page(browser):
    page = SchemeAppealPage(browser, SchemeAppealsLocators.URL)
    page.open()
    page.should_be_major_elements()


"""Доступность элементов на странице создания договора купли/продажи"""
@pytest.mark.smoke
def test_should_be_major_elements_generate_contract_page(browser):
    page = GenerateContractPage(browser, GenerateContractLocators.URL)
    page.open()
    page.should_be_major_elements()


"""Доступность элементов на странице Страхование"""
@pytest.mark.smoke
def test_should_be_major_elements_inshurance_page(browser):
    page = InshurancePage(browser, InsurancePageLocators.URL)
    page.open()
    page.should_be_major_elements()


"""Доступность элементов на странице Настройка уведомлений"""
@pytest.mark.smoke
def test_should_be_major_elements_notification_page(browser):
    page = BasePage(browser, BasePageLocators.URL)
    page.open()
    page.login()
    page_notification = NotificationPage(browser, NotificationLocators.URL)
    page_notification.open()
    page_notification.should_be_major_elements()


"""Проверяет корректный переход в личный кабинет Мосру"""
@pytest.mark.smoke
def test_should_go_to_mosru_lk(browser):
    page = BasePage(browser, BasePageLocators.URL)
    page.open()
    page.login()
    page_notification = NotificationPage(browser, NotificationLocators.URL)
    page_notification.open()
    page_notification.go_to_mosru()