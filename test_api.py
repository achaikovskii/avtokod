import pytest
import requests
from .pages.locators import APILocators


@pytest.mark.api
def test_get_new_fines():
    assert requests.post(APILocators.URL_GET_FINES, json=APILocators.URL_GET_FINES_JSON).status_code == 200, \
        'API get_new_fines не доступно'


@pytest.mark.api
def test_get_paid_fines():
    assert requests.post(APILocators.URL_GET_PAID_FINES, json=APILocators.URL_GET_FINES_JSON).status_code == 200, \
        'API get_paid_fines не доступно'


@pytest.mark.api
def test_get_photo():
    assert requests.post(APILocators.URL_GET_PHOTO, json=APILocators.URL_GET_PHOTO_JSON).status_code == 200, \
        'API get photo не доступно'
