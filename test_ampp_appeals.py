import pytest
import time
from .pages.base_page import BasePage
from .pages.ampp_appeals import AmppAppeals
from .pages.locators import AmppAppealsLocators
from .pages.locators import BasePageLocators


"""Доступность всех причин обжалования для страницы жалоб АМПП"""
@pytest.mark.smoke
def test_should_be_all_reasons_ampp_for_appeals(browser):
    page = AmppAppeals(browser, AmppAppealsLocators.URL)
    page.open()
    page.should_be_all_reasons_for_appeals()


"""Для перехода на шаг 2 должна быть корректно заполненная форма"""
def test_should_be_available_button_by_correct_form_step_one(browser):
    page = BasePage(browser, BasePageLocators.URL)
    page.open()
    page.login()
    appeal_page = AmppAppeals(browser, AmppAppealsLocators.URL)
    appeal_page.open()
    appeal_page.go_to_appeal_ap()
    appeal_page.click_ok_on_the_info_page()
    appeal_page.fill_appeal_data_step_one()
    appeal_page.go_next_step()
    appeal_page.should_be_open_form_two()


"""Если форма не заполнена, то кнопка не активна"""
def test_should_be_disable_button_by_incorrect_form_step_one(browser):
    page = BasePage(browser, BasePageLocators.URL)
    page.open()
    page.login()
    appeal_page = AmppAppeals(browser, AmppAppealsLocators.URL)
    appeal_page.open()
    appeal_page.go_to_appeal_ap()
    appeal_page.click_ok_on_the_info_page()
    appeal_page.check_disable_button_next_step_one()
