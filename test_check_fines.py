import pytest
from time import sleep
from .pages.base_page import BasePage
from .pages.check_fines import CheckFines
from .pages.locators import CheckFinesLocators
from .pages.locators import BasePageLocators


"""Отображение главных элементов на странице поиска штрафов"""
@pytest.mark.smoke
def test_guest_check_fines_elements_is_available(browser):
    page = CheckFines(browser, CheckFinesLocators.URL)
    page.open()
    page.should_be_major_elements()


"""Проверяет переход гостя на форму авторизации запроса на проверку штрафа по УИН"""
def test_guest_check_fines_uin(browser):
    page = CheckFines(browser, CheckFinesLocators.URL)
    page.open()
    page.check_fine_uin()
    page.check_result_for_guest()

"""Должен быть редирект на Мои платежи при проверке по УИН"""
@pytest.mark.smoke
def test_user_check_fines_uin(browser):
    page = BasePage(browser, BasePageLocators.URL)
    page.open()
    page.login()
    page_fines = CheckFines(browser, CheckFinesLocators.URL)
    page_fines.open()
    page_fines.check_fine_uin()
    result_page = CheckFines(browser, browser.current_url)
    result_page.check_redirect()


"""Должен быть редирект на Мои платежи при проверке по ВУ"""
def test_user_check_fines_vu(browser):
    page = BasePage(browser, BasePageLocators.URL)
    page.open()
    page.login()
    page_fines = CheckFines(browser, CheckFinesLocators.URL)
    page_fines.open()
    page_fines.check_fine_vu()
    result_page = CheckFines(browser, browser.current_url)
    result_page.check_redirect()


"""Должен быть редирект на Мои платежи при проверке по СТС"""
def test_user_check_fines_sts(browser):
    page = BasePage(browser, BasePageLocators.URL)
    page.open()
    page.login()
    page_fines = CheckFines(browser, CheckFinesLocators.URL)
    page_fines.open()
    page_fines.check_fine_sts()
    result_page = CheckFines(browser, browser.current_url)
    result_page.check_redirect()


"""Проверка сообщения при некорректном УИН"""
def test_check_invalid_uin(browser):
    page = BasePage(browser, BasePageLocators.URL)
    page.open()
    page.login()
    page_fines = CheckFines(browser, CheckFinesLocators.URL)
    page_fines.open()
    page_fines.should_be_message_invalid_dap()


"""Проверка сообщения при некорректном ВУ"""
def test_check_invalid_vu(browser):
    page = BasePage(browser, BasePageLocators.URL)
    page.open()
    page.login()
    page_fines = CheckFines(browser, CheckFinesLocators.URL)
    page_fines.open()
    sleep(3)
    page_fines.should_be_message_invalid_vu()

"""Проверка сообщения при некорректном СТС"""
def test_check_invalid_sts(browser):
    page = BasePage(browser, BasePageLocators.URL)
    page.open()
    page.login()
    page_fines = CheckFines(browser, CheckFinesLocators.URL)
    page_fines.open()
    page_fines.should_be_message_invalid_sts()
